<?php

class Dependents {

        private $wpdb;
        private $table_name;

        public function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;
            $this->table_name = $this->wpdb->prefix . "dependents";
        }

        public function format($data){
            return isset($data) && trim($data) !== "" ? $data : 'null';
        }

        public function format_int($data){
            $aux = trim(str_replace('"', "", $data));
            return intval($aux);
        }

        public function format_data($data){
            $aux = trim(str_replace('"', "", $data));
            $data = implode('-', array_reverse(explode('/', $aux)));
            return '"' . $data . '"';
        }

        public function store($file_path){
            
            $aux = 0;
            $query = null;
            $fn = fopen($file_path, "r");

            if ($fn) {

                $delete = $this->wpdb->query("TRUNCATE TABLE {$this->table_name}");

                $query = "INSERT INTO {$this->table_name}
                            (beneficiary_id,
                            id,
                            name,
                            admission,
                            lack, 
                            degree) VALUES ";

                while (($line = fgets($fn)) !== false) {
                    
                    $aux++;
                    $dependent = explode(';', utf8_encode($line));

                    $data = [
                        isset($dependent[0]) ? $this->format_int($dependent[0]) : 'null', 
                        isset($dependent[1]) ? $this->format_int($dependent[1]) : 'null', 
                        isset($dependent[2]) ? $this->format($dependent[2]) : 'null', 
                        isset($dependent[3]) ? $this->format_data($dependent[3]) : 'null', 
                        isset($dependent[4]) ? $this->format_data($dependent[4]) : 'null',
                        isset($dependent[5]) ? $this->format($dependent[5]) : 'null'
                    ];

                    $query .= '(' . implode(",", $data) . '),';
                }

                $query = substr($query,0,-1) . ';';

            } else {
                return false;
            }

            if(!isset($query)) return false;

            $this->wpdb->query($query);
            return $this->wpdb->last_error == "" ? true : false;
        }

    }
?>