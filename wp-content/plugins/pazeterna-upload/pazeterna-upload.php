<?php
  /*******************************
   * Plugin Name:  Paz Eterna Upload
   * Description:  Paz Eterna Upload
   * Version:      1.0
   * Author:       Marcos Soares
   * License:      MIT
   *******************************/

  // Add menu
  function pazeterna_menu() {
    add_menu_page("Paz Eterna", "Paz Eterna","manage_options", "myplugin", "uploadfile");
  }
  
  add_action("admin_menu", "pazeterna_menu");
  
  function uploadfile(){
    include "uploadfile.php";
  }

?>