<?php 
/*
Template Name: Carregar Arquivos
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carregar Arquivos</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <?php

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        include "load_beneficiaries.php";
        include "load_dependents.php";

        if(isset($_FILES['beneficiaries']) || isset($_FILES['beneficiaries'])){
        
            $dir = wp_upload_dir();

            if(isset($_FILES['beneficiaries']) && $_FILES['beneficiaries']['size'] > 0){
                //Beneficiários
                $infoBeneficiaries = pathinfo($_FILES['beneficiaries']['name']);
                $extBeneficiaries = $infoBeneficiaries['extension'];
                $nameBeneficiaries = "beneficiaries.".$extBeneficiaries; 
                $targetBeneficiaries = $dir['basedir'] . '/pazeterna/'. $nameBeneficiaries;
                $uploadBeneficiaries = move_uploaded_file( $_FILES['beneficiaries']['tmp_name'], $targetBeneficiaries);
                
                if($uploadBeneficiaries){
                    $beneficiaries = new Beneficiaries();
                    if($beneficiaries->store($targetBeneficiaries)){
                        echo '<script> alert("Beneficiários enviados com sucesso"); </script>';
                    } else {
                        echo '<script> alert("Erro ao salvar beneficiarios na base de dados"); </script>';
                    }

                } else {
                    echo '<script> alert("Erro ao salvar arquivo de Beneficitarios"); </script>';
                }
            }

            if(isset($_FILES['dependents']) && $_FILES['dependents']['size'] > 0){
                 
                //Dependentes
                $infoDependents = pathinfo($_FILES['dependents']['name']);
                $extDependents = $infoDependents['extension'];
                $nameDependents = "dependents.".$extDependents; 
                $targetDependents = $dir['basedir'] . '/pazeterna/'. $nameDependents;
                $uploadDependents = move_uploaded_file( $_FILES['dependents']['tmp_name'], $targetDependents);

                if($uploadDependents){
                    $dependents = new Dependents();
                    if($dependents->store($targetDependents)){
                        echo '<script> alert("Dependentes enviados com sucesso"); </script>';
                    } else {
                        echo '<script> alert("Erro ao salvar dependentes na base de dados"); </script>';
                    }

                } else {
                    echo '<script> alert("Erro ao salvar arquivo de Dependentes"); </script>';
                }
            }
        }
    ?>
</head>

<body>
    <div class="container">
        
        <div class="row">
            <div class="mx-auto mt-4">
                <h3>Enviar arquivos - Paz Eterna</h3>
                <p>
                    Este plugin foi desenvolvido para enviar os arquivos de <b>Beneficiários</b> e <b>Dependentes</b> da Paz Eterna.
                    Os arquivos podem ser enviados <b>ao mesmo tempo ou separadamente</b>.
                </p>
                <p>Os arquivos podem demorar um pouco para serem processados, <b>aguarde a mensagem de retorno.</b></p>
            </div>
        </div>

        <div class="row">
            <div class="mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Envio de arquivos</h5>
                        <form enctype="multipart/form-data" class="form-file" method="post" onsubmit="loadButton()">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Arquivo de Beneficiários</label>
                                <input type="file" class="form-control-file" name="beneficiaries" id="beneficiaries">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Arquivo de Dependentes</label>
                                <input type="file" class="form-control-file" name="dependents" id="dependents"> 
                            </div>
                            <hr class="my-4">
                            <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase btn-send" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Enviando... (Pode demorar)">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script>
    function loadButton() {
        var button = document.getElementsByClassName("btn-send")[0];
        var textLoadingButton = button.dataset.loadingText;
        button.innerHTML = textLoadingButton;
        button.disabled = true;
    }
</script>

</html>