<?php

class Beneficiaries {

        private $wpdb;
        private $table_name;

        public function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;
            $this->table_name = $this->wpdb->prefix . "beneficiaries";
        }

        public function format($data){
            return isset($data) && trim($data) !== "" ? $data : 'null';
        }

        public function format_int($data){
            $aux = trim(str_replace('"', "", $data));
            return intval($aux);
        }

        public function format_data($data){
            $aux = trim(str_replace('"', "", $data));
            $data = implode('-', array_reverse(explode('/', $aux)));
            return '"' . $data . '"';
        }

        public function store($file_path){
            
            $aux = 0;
            $query = null;
            $fn = fopen($file_path, "r");

            if ($fn) {

                $delete = $this->wpdb->query("TRUNCATE TABLE {$this->table_name}");

                $query = "INSERT INTO {$this->table_name}
                            (id,
                            password,
                            name,
                            cpf,
                            address,
                            complement,
                            neighborhood,
                            city,
                            fiscal_code,
                            phone,
                            cell_phone,
                            plan,
                            admission,
                            lack,
                            antepenultimate_payment_format,
                            antepenultimate_payment,
                            penultimate_payment_format,
                            penultimate_payment,
                            last_payment_format,
                            last_payment) VALUES ";

                while (($line = fgets($fn)) !== false) {
                    
                    $aux++;
                    $beneficiary = explode(';', utf8_encode($line));

                    $data = [
                        isset($beneficiary[0])  ? $this->format_int($beneficiary[0]) : 'null', 
                        isset($beneficiary[1])  ? $this->format_int($beneficiary[1]) : 'null', 
                        isset($beneficiary[2])  ? $this->format($beneficiary[2]) : 'null', 
                        isset($beneficiary[3])  ? $this->format($beneficiary[3]) : 'null', 
                        isset($beneficiary[4])  ? $this->format($beneficiary[4]) : 'null',
                        isset($beneficiary[5])  ? $this->format($beneficiary[5]) : 'null',
                        isset($beneficiary[6])  ? $this->format($beneficiary[6]) : 'null',
                        isset($beneficiary[7])  ? $this->format($beneficiary[7]) : 'null', 
                        isset($beneficiary[8])  ? $this->format($beneficiary[8]) : 'null', 
                        isset($beneficiary[9])  ? $this->format($beneficiary[9]) : 'null', 
                        isset($beneficiary[10]) ? $this->format($beneficiary[10]) : 'null', 
                        isset($beneficiary[11]) ? $this->format($beneficiary[11]) : 'null', 
                        isset($beneficiary[12]) ? $this->format_data($beneficiary[12]) : 'null',
                        isset($beneficiary[13]) ? $this->format_data($beneficiary[13]) : 'null',
                        isset($beneficiary[14]) ? $this->format($beneficiary[14]) : 'null',
                        isset($beneficiary[15]) ? $this->format_data($beneficiary[15]) : 'null',
                        isset($beneficiary[16]) ? $this->format($beneficiary[16]) : 'null', 
                        isset($beneficiary[17]) ? $this->format_data($beneficiary[17]) : 'null',
                        isset($beneficiary[18]) ? $this->format($beneficiary[18]) : 'null',
                        isset($beneficiary[19]) ? $this->format_data($beneficiary[19]) : 'null'
                    ];

                    $query .= '(' . implode(",", $data) . '),';

                }

                $query = substr($query,0,-1) . ';';

            } else {
                return false;
            }

            if(!isset($query)) return false;

            $this->wpdb->query($query);
            return $this->wpdb->last_error == "" ? true : false;
        }

    }
?>