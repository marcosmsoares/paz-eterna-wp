<?php 
/*
Template Name: Carregar Arquivos
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carregar Arquivos</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <?php

        var_dump(isset($_FILES['beneficiaries']));

        if(isset($_FILES['beneficiaries']) && isset($_FILES['dependents'])){

            die('oi');

            //include_once get_template_directory_uri() . '/load-beneficiaries.php';

            $dir = wp_upload_dir();

            //Beneficiários
            $infoBeneficiaries = pathinfo($_FILES['beneficiaries']['name']);
            $extBeneficiaries = $infoBeneficiaries['extension'];
            $nameBeneficiaries = "beneficiaries.".$ext; 
            $targetBeneficiaries = $dir['basedir'] . '/pazeterna/'. $extBeneficiaries;
            $uploadBeneficiaries = move_uploaded_file( $_FILES['beneficiaries']['tmp_name'], $targetBeneficiaries);

            //Dependentes
            $infoDependents = pathinfo($_FILES['dependents']['name']);
            $extDependents = $infoDependents['extension'];
            $nameDependents = "dependents.".$ext; 
            $targetDependents = $dir['basedir'] . '/pazeterna/'. $extDependents;
            $uploadDependents = move_uploaded_file( $_FILES['dependents']['tmp_name'], $targetBeneficiaries);

            if($uploadBeneficiaries && $uploadDependents){

                $beneficiaries = new Beneficiaries();
                if($beneficiaries->store($targetBeneficiaries)){
                    echo '<script> alert("Arquivos enviados com sucesso")</script>';
                } else {
                    echo '<script> alert("Ocorreu um erro")</script>';
                }

            } else {
                echo '<script> alert("Ocorreu um erro")</script>';
            }
        }
    ?>
</head>

<body>
    <?php get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Envio de arquivos</h5>
                        <form enctype="multipart/form-data" class="form-file" method="post" onsubmit="loadButton()">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Arquivo de Beneficiários</label>
                                <input type="file" class="form-control-file" name="beneficiaries" id="beneficiaries" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Arquivo de Dependentes</label>
                                <input type="file" class="form-control-file" name="dependents" id="dependents"> 
                            </div>
                            <hr class="my-4">
                            <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase btn-send" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Enviando...">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
</body>

<script>
    function loadButton() {
        var button = document.getElementsByClassName("btn-send")[0];
        var textLoadingButton = button.dataset.loadingText;
        button.innerHTML = textLoadingButton;
        button.disabled = true;
    }
</script>

</html>