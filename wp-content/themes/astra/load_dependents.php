<?php 
/*
Template Name: Load File Dependentes
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carregar Arquivos</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <?php
        global $wpdb;
        $table_name = $wpdb->prefix . "dependents";
    ?>
</head>

<body>
    <?php get_header(); ?>
    <div class="container">
        <?php

            function format($data){
                return isset($data) && trim($data) !== "" ? $data : 'null';
            }

            function format_int($data){
                $aux = trim(str_replace('"', "", $data));
                return intval($aux);
            }

            function format_data($data){
                $aux = trim(str_replace('"', "", $data));
                $data = implode('-', array_reverse(explode('/', $aux)));
                return '"' . $data . '"';
            }

            function is_valid($data){
                return isset($data) && trim($data) !== "" ? $data : null;
            }

            $aux = 0;
            $query = null;
            $file_path = 'wp-content/themes/astra/assets/file/Dependentes.Txt';
            $fn = fopen($file_path, "r");

            if ($fn) {

                $delete = $wpdb->query("TRUNCATE TABLE $table_name");

                $query = "INSERT INTO {$table_name}
                            (beneficiary_id,
                            id,
                            name,
                            admission,
                            lack, 
                            degree) VALUES ";

                while (($line = fgets($fn)) !== false) {
                    
                    $aux++;
                    $dependent = explode(';', utf8_encode($line));

                    $data = [
                        is_valid($dependent[0]) ? format_int($dependent[0]) : 'null', 
                        is_valid($dependent[1]) ? format_int($dependent[1]) : 'null', 
                        is_valid($dependent[2]) ? format($dependent[2]) : 'null', 
                        is_valid($dependent[3]) ? format_data($dependent[3]) : 'null', 
                        is_valid($dependent[4]) ? format_data($dependent[4]) : 'null',
                        is_valid($dependent[5]) ? format($dependent[5]) : 'null'
                    ];

                    $query .= '(' . implode(",", $data) . '),';
                }

                $query = substr($query,0,-1) . ';';

            } else {
                die('file not found');
            }

            if($query){
                $wpdb->query($query);
                echo 'foram salvos ' . $aux . ' registros';
            }
        ?>

    </div>
    <?php get_footer(); ?>
</body>

</html>