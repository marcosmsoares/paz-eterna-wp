<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

?>
			<?php astra_content_bottom(); ?>

			</div> <!-- ast-container -->

		</div><!-- #content -->

		<?php astra_content_after(); ?>

		<?php astra_footer_before(); ?>

		<?php astra_footer(); ?>

		<?php astra_footer_after(); ?>

	</div><!-- #page -->

	<?php astra_body_bottom(); ?>

	<?php wp_footer(); ?>

	<script>
		jQuery('#compare-plan').click(function(){
			
			var plan1 = jQuery('#plano-1').is(":checked");
			var plan2 = jQuery('#plano-2').is(":checked");
			var plan3 = jQuery('#plano-3').is(":checked");
			var plan4 = jQuery('#plano-4').is(":checked");
			var plan5 = jQuery('#plano-5').is(":checked");

			var compare = {
				'plano-extra' : plan1,
				'plano-economico' : plan2,
				'plano-interior' : plan3,
				'plano-basico' : plan4,
				'plano-especial' : plan5
			};

			var aux = '';

			jQuery.each(compare, function(k, v) {
				if(v){
					aux += k + "&";
				}
			});

			if(aux == ''){
				alert('Nenhum plano selecionado. Clique em "Compare" nos planos desejados');
			} else {
				aux = aux.substring(0, aux.length - 1);
				window.location.href = "/comparar-planos?" + aux;
			}

		});
	</script>

	</body>
</html>
