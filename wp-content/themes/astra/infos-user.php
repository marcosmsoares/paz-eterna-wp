<?php 
/*
Template Name: Infos User
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informações do Usuário</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
      p {
        margin-bottom: 0.75em!important;
      }
    </style>
    <!-- <script src="https://kit.fontawesome.com/8686281194.js" crossorigin="anonymous"></script> -->
    
    <?php

        session_start();
        $user = isset($_SESSION['beneficiary']) ? $_SESSION['beneficiary'] : null;

        if(!isset($user)){
            header("Location: /login/"); 
        }

        //var_dump($user);

        function format($data){
            return isset($data) && trim($data) !== "" ? $data : 'Não informado';
        }

        function format_data($data){
            return isset($data) && trim($data) !== "" ? date("d/m/Y", strtotime($data)) : 'Não informado';
        }

    ?>
</head>

<body>
    <?php get_header(); ?>
    <div class="container">
        <div class="row mt-4">
            <div class="col-sm-6">
                <p>
                    Olá, <b><?= isset($user->name) ? $user->name : "" ?></b><br>
                    <a href="/login?logout=true">Sair da minha conta</a>
                </p>
            </div>
            <div class="col-sm-6">
                <p>
                    <button type="button" class="btn btn-info float-right">2ª VIA DE BOLETO</button>
                </p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="card text-white bg-info col-sm m-2">
                <div class="card-header"><i class="fa fa-clipboard"></i> Meu plano</div>
                <div class="card-body">
                    <p class="card-text"><?= format($user->plan) ?></p>
                </div>
            </div>
            <div class="card text-white bg-warning col-sm m-2">
                <div class="card-header"><i class="fa fa-calendar"></i> Admissão do plano</div>
                <div class="card-body">
                    <p class="card-text"><?= format_data($user->admission) ?></p>
                </div>
            </div>
            <div class="card text-white bg-success col-sm m-2">
                <div class="card-header"><i class="fa fa-money"></i> Último pagamento</div>
                <div class="card-body">
                    <p class="card-text"><?= format($user->last_payment_format) ?></p>
                </div>
            </div>
            <div class="card text-white bg-secondary col-sm m-2">
                <div class="card-header"><i class="fa fa-users"></i> Dependentes</div>
                <div class="card-body">
                    <p class="card-text"><?= count($user->dependents) . ' Dependente(s)' ?></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 mt-4">
                <h4 class="display-6">Meus dados</h4>
            </div>
            <div class="jumbotron p-4 col-sm m-2">
                <!-- <h1 class="display-6">Meus dados</h1>
                <hr class="my-4"> -->

                <div class="row">
                    <div class="col-sm-3">
                        <p class="font-weight-bold"><i class="fa fa-user"></i> Dados pessoais</p>
                        <hr class="my-2">
                        <p>ID: <?= format($user->id) ?></p>
                        <p>Nome: <?= format($user->name) ?></p>
                        <p>Telefone: <?= format($user->phone) ?></p>
                        <p>Celular: <?= format($user->cell_phone) ?></p>
                    </div>

                    <div class="col-sm-3">
                        <p class="font-weight-bold"><i class="fa fa-map-marker"></i> Endereço</p>
                        <hr class="my-2">
                        <p>Cidade: <?= format($user->city) ?></p>
                        <p>CEP: <?= format($user->fiscal_code) ?></p>
                        <p>Logradouro: <?= format($user->address) ?></p>
                        <p>Bairro: <?= format($user->neighborhood) ?></p>
                        <p>Complemento: <?= format($user->complement) ?></p>
                    </div>

                    <div class="col-sm-3">
                        <p class="font-weight-bold"><i class="fa fa-clipboard"></i> Plano</p>
                        <hr class="my-2">
                        <p>Tipo do plano: <?= format($user->plan) ?></p>
                        <p>Admissão: <?= format_data($user->admission) ?></p>
                        <p>Carência: <?= format_data($user->lack) ?></p>
                    </div>

                    <div class="col-sm-3">
                        <p class="font-weight-bold"><i class="fa fa-money"></i> Pagamento</p>
                        <hr class="my-2">
                        <p>Antepenúltimo: <?= format($user->antepenultimate_payment_format) ?></p>
                        <p>Penúltimo: <?= format($user->penultimate_payment_format) ?></p>
                        <p>Último: <?= format($user->last_payment_format) ?></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 mt-4">
                <h4 class="display-6">Dependentes</h4>
            </div>

            <div class="p-4 col-sm mr-2 ml-2">
                <div class="row">
                    <?php if(count($user->dependents) > 0): ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Data de admissão</th>
                                <th scope="col">Data de carência</th>
                                <th scope="col">Grau de parentesco</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($user->dependents as $key => $dependent): ?>
                            <tr>
                                <th scope="row"><?= $key + 1 ?></th>
                                <td><?= format($dependent->name) ?></td>
                                <td><?= format_data($dependent->admission) ?></td>
                                <td><?= format_data($dependent->lack) ?></td>
                                <td><?= format($dependent->degree) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php else: ?>
                        <span class="bold-text">Não possui dependentes.</span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <br>
    <?php get_footer(); ?>
</body>
</html>