<?php 
/*
Template Name: Comparar Planos
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comparar Planos</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        
        .custom-bg{
            background-color: #094699!important;
        }

    </style>
    <?php
        $plan1 = isset($_GET['plano-extra']);
        $plan2 = isset($_GET['plano-economico']);
        $plan3 = isset($_GET['plano-interior']);
        $plan4 = isset($_GET['plano-basico']);
        $plan5 = isset($_GET['plano-especial']);
    ?>

</head>

<body>
    <?php get_header(); ?>
    <div class="container">
        <div class="row mt-4">
            <div class="col-sm-12">
                <h2>Comparar planos</h2>
                <br>
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <?php if($plan1): ?><th class="titulo h3 custom-bg text-white">Plano Extra</th><?php endif; ?>
                            <?php if($plan2): ?><th class="titulo h3 custom-bg text-white">Plano Econômico</th></th><?php endif; ?>
                            <?php if($plan3): ?><th class="titulo h3 custom-bg text-white">Simples Interior</th></th><?php endif; ?>
                            <?php if($plan4): ?><th class="titulo h3 custom-bg text-white">Plano Básico</th></th><?php endif; ?>
                            <?php if($plan5): ?><th class="titulo h3 custom-bg text-white">Plano Especial</th></th><?php endif; ?>
                        </tr>

                        <tr>
                            <?php if($plan1): ?><td>Funeral primeira classe</td><?php endif; ?>
                            <?php if($plan2): ?><td>Funeral primeira classe</td><?php endif; ?>
                            <?php if($plan3): ?><td>Funeral primeira classe</td><?php endif; ?>
                            <?php if($plan4): ?><td>Funeral primeira classe</td><?php endif; ?>
                            <?php if($plan5): ?><td>Urna envernizada com visor padrão Vip</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Urna envernizada com visor padrão Plano Extra</td><?php endif; ?>
                            <?php if($plan2): ?><td>Urna envernizada com visor padrão Plano Econômico</td><?php endif; ?>
                            <?php if($plan3): ?><td>Urna envernizada com visor padrão Plano Simples Interior</td><?php endif; ?>
                            <?php if($plan4): ?><td>Urna envernizada com visor padrão Plano Básico</td><?php endif; ?>
                            <?php if($plan5): ?><td>Funeral Vip (Especial)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Flores (1 pacote) para ornamentação da urna mortuária (Busto)</td><?php endif; ?>
                            <?php if($plan2): ?><td>Flores (1 pacote) para ornamentação da urna mortuária (Busto)</td><?php endif; ?>
                            <?php if($plan3): ?><td>Flores (1 pacote) para ornamentação da urna mortuária (Busto)</td><?php endif; ?>
                            <?php if($plan4): ?><td>Flores (1 pacote) para ornamentação da urna mortuária (Busto)</td><?php endif; ?>
                            <?php if($plan5): ?><td>Flores para ornamentação completa da urna mortuária</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Câmara ardente</td><?php endif; ?>
                            <?php if($plan2): ?><td>Câmara ardente</td><?php endif; ?>
                            <?php if($plan3): ?><td>Câmara ardente</td><?php endif; ?>
                            <?php if($plan4): ?><td>Mortalha</td><?php endif; ?>
                            <?php if($plan5): ?><td>Mesa e impressos para registro de presença</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Mortalha</td><?php endif; ?>
                            <?php if($plan2): ?><td>Mortalha</td><?php endif; ?>
                            <?php if($plan3): ?><td>Mortalha</td><?php endif; ?>
                            <?php if($plan4): ?><td>Câmara ardente</td><?php endif; ?>
                            <?php if($plan5): ?><td>Câmara ardente Vip</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Mesa e impressos para registro de presença</td><?php endif; ?>
                            <?php if($plan2): ?><td>Mesa e impressos para registro de presença</td><?php endif; ?>
                            <?php if($plan3): ?><td>Mesa e impressos para registro de presença</td><?php endif; ?>
                            <?php if($plan4): ?><td>Mesa e impressos para registro de presença</td><?php endif; ?>
                            <?php if($plan5): ?><td>Mortalha</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Edredom</td><?php endif; ?>
                            <?php if($plan2): ?><td>Edredom</td><?php endif; ?>
                            <?php if($plan3): ?><td>Edredom</td><?php endif; ?>
                            <?php if($plan4): ?><td>Tapete</td><?php endif; ?>
                            <?php if($plan5): ?><td>Celebração de Corpo presente</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Tapete</td><?php endif; ?>
                            <?php if($plan2): ?><td>Tapete</td><?php endif; ?>
                            <?php if($plan3): ?><td>Tapete</td><?php endif; ?>
                            <?php if($plan4): ?><td>Edredom</td><?php endif; ?>
                            <?php if($plan5): ?><td>Tapete</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Saiote</td><?php endif; ?>
                            <?php if($plan2): ?><td>Saiote</td><?php endif; ?>
                            <?php if($plan3): ?><td>Saiote</td><?php endif; ?>
                            <?php if($plan4): ?><td>Saiote</td><?php endif; ?>
                            <?php if($plan5): ?><td>Edredom</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Velas</td><?php endif; ?>
                            <?php if($plan2): ?><td>Velas</td><?php endif; ?>
                            <?php if($plan3): ?><td>Velas</td><?php endif; ?>
                            <?php if($plan4): ?><td>Velas</td><?php endif; ?>
                            <?php if($plan5): ?><td>Saiote</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Cortinado</td><?php endif; ?>
                            <?php if($plan2): ?><td>Cortinado</td><?php endif; ?>
                            <?php if($plan3): ?><td>Cortinado</td><?php endif; ?>
                            <?php if($plan4): ?><td>Cortinado</td><?php endif; ?>
                            <?php if($plan5): ?><td>Velas</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Kit café</td><?php endif; ?>
                            <?php if($plan2): ?><td>Kit café</td><?php endif; ?>
                            <?php if($plan3): ?><td>Registro da Declaração de óbito (Guia de Sepultamento)</td><?php endif; ?>
                            <?php if($plan4): ?><td>Kit café</td><?php endif; ?>
                            <?php if($plan5): ?><td>Cortinado</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Coroa de Flores Naturais (Ref. B)</td><?php endif; ?>
                            <?php if($plan2): ?><td>Coroa de Flores Naturais (Ref. B)</td><?php endif; ?>
                            <?php if($plan3): ?><td>Kit café</td><?php endif; ?>
                            <?php if($plan4): ?><td>Registro da Declaração de óbito (Guia de Sepultamento)</td><?php endif; ?>
                            <?php if($plan5): ?><td>Kit café</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Registro da Declaração de óbito (Guia de Sepultamento)</td><?php endif; ?>
                            <?php if($plan2): ?><td>Registro da Declaração de óbito (Guia de Sepultamento)</td><?php endif; ?>
                            <?php if($plan3): ?><td>Coroa de Flores Naturais (Ref. B)</td><?php endif; ?>
                            <?php if($plan4): ?><td>Coroa de Flores Naturais (Ref. B)</td><?php endif; ?>
                            <?php if($plan5): ?><td>Taxa de sepultamento (Abertura de jazigo)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Certidão de óbito</td><?php endif; ?>
                            <?php if($plan2): ?><td>Certidão de óbito</td><?php endif; ?>
                            <?php if($plan3): ?><td>Certidão de óbito</td><?php endif; ?>
                            <?php if($plan4): ?><td>Certidão de óbito</td><?php endif; ?>
                            <?php if($plan5): ?><td>Registro da Declaração de óbito (Guia de Sepultamento)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Ônibus ou Micro ônibus (Até 70 km rodados)</td><?php endif; ?>
                            <?php if($plan2): ?><td>Ônibus ou Micro ônibus (Até 70 km rodados)</td><?php endif; ?>
                            <?php if($plan3): ?><td>Auto fúnebre (Até 200 km rodados)</td><?php endif; ?>
                            <?php if($plan4): ?><td>Auto fúnebre (Até 200 km rodados)</td><?php endif; ?>
                            <?php if($plan5): ?><td>Coroa de Flores Naturais (Especial)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Auto fúnebre (Até 200 km rodados)</td><?php endif; ?>
                            <?php if($plan2): ?><td>Auto fúnebre (Até 200 km rodados)</td><?php endif; ?>
                            <?php if($plan3): ?><td>Sala de velório Paz Eterna com 50% de desconto no preço de tabela</td><?php endif; ?>
                            <?php if($plan4): ?><td>Sala de velório Paz Eterna com 50% de desconto no preço de tabela</td><?php endif; ?>
                            <?php if($plan5): ?><td>Certidão de óbito</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>Higienização do corpo e Necromaquiagem</td><?php endif; ?>
                            <?php if($plan2): ?><td>Higienização do corpo e Necromaquiagem</td><?php endif; ?>
                            <?php if($plan3): ?><td>Higienização do corpo e Necromaquiagem</td><?php endif; ?>
                            <?php if($plan4): ?><td>Ônibus ou Micro ônibus (Até 70 km rodados)</td><?php endif; ?>
                            <?php if($plan5): ?><td>Auto fúnebre (Até 200 km rodados)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td>30 Lembrancinhas</td><?php endif; ?>
                            <?php if($plan2): ?><td>30 Lembrancinhas</td><?php endif; ?>
                            <?php if($plan3): ?><td>60 Lembrancinhas</td><?php endif; ?>
                            <?php if($plan4): ?><td>Higienização do corpo e Necromaquiagem</td><?php endif; ?>
                            <?php if($plan5): ?><td>Sala de velório Paz Eterna</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td></td><?php endif; ?>
                            <?php if($plan2): ?><td></td><?php endif; ?>
                            <?php if($plan3): ?><td></td><?php endif; ?>
                            <?php if($plan4): ?><td>Cremação com 50% de desconto no preço de tabela</td><?php endif; ?>
                            <?php if($plan5): ?><td>Ônibus ou Micro ônibus (Até 70 km rodados)</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td></td><?php endif; ?>
                            <?php if($plan2): ?><td></td><?php endif; ?>
                            <?php if($plan3): ?><td></td><?php endif; ?>
                            <?php if($plan4): ?><td>50 Lembrancinhas</td><?php endif; ?>
                            <?php if($plan5): ?><td>Higienização do corpo e Necromaquiagem</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td></td><?php endif; ?>
                            <?php if($plan2): ?><td></td><?php endif; ?>
                            <?php if($plan3): ?><td></td><?php endif; ?>
                            <?php if($plan4): ?><td></td><?php endif; ?>
                            <?php if($plan5): ?><td>Tanatopraxia</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td></td><?php endif; ?>
                            <?php if($plan2): ?><td></td><?php endif; ?>
                            <?php if($plan3): ?><td></td><?php endif; ?>
                            <?php if($plan4): ?><td></td><?php endif; ?>
                            <?php if($plan5): ?><td>Cremação</td><?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td></td><?php endif; ?>
                            <?php if($plan2): ?><td></td><?php endif; ?>
                            <?php if($plan3): ?><td></td><?php endif; ?>
                            <?php if($plan4): ?><td></td><?php endif; ?>
                            <?php if($plan5): ?><td>100 Lembrancinhas</td><?php endif; ?>
                        </tr>

                        <tr>
                            <?php 
                            if($plan1): ?>
                            <td>
                                <p>R$<span class="h1"> 31,02</span></p>
                            </td>
                            <?php endif; ?>
                            <?php if($plan2): ?>
                            <td>
                                <p>R$<span class="h1"> 41,37</span></p>
                            </td
                            ><?php endif; ?>
                            <?php if($plan3): ?>
                            <td>
                                <p>R$<span class="h1"> 41,37</span></p>
                            </td>
                            <?php endif; ?>
                            <?php if($plan4): ?>
                            <td>
                                <p>R$<span class="h1"> 62,05</span></p>
                            </td>
                            <?php endif; ?>
                            <?php if($plan5): ?>
                            <td>
                                <p>R$<span class="h1"> 133,04</span></p>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <?php if($plan1): ?><td><a href="/plano-extra"><button type="button" class="btn btn-primary btn-block custom-bg">Assinar este plano</button></a></td><?php endif; ?>
                            <?php if($plan2): ?><td><a href="/plano-economico"><button type="button" class="btn btn-primary btn-block custom-bg">Assinar este plano</button></a></td><?php endif; ?>
                            <?php if($plan3): ?><td><a href="/plano-simples-interior"><button type="button" class="btn btn-primary btn-block custom-bg">Assinar este plano</button></a></td><?php endif; ?>
                            <?php if($plan4): ?><td><a href="/plano-basico"><button type="button" class="btn btn-primary btn-block custom-bg">Assinar este plano</button></a></td><?php endif; ?>
                            <?php if($plan5): ?><td><a href="/plano-especial"><button type="button" class="btn btn-primary btn-block custom-bg">Assinar este plano</button></a></td><?php endif; ?>
                        </tr>
                    </tbody>
                </table>

                <br>

                <div class="col-sm-12 col-md-offset-4 text-center">
                    <a href="/planos">
                        <button type="button" class="btn btn-outline-secondary">Voltar aos planos</button>
                    </a>
                </div>

                <div class="clearfix"></div>

                <br>

            </div>
        </div>
    </div>
    <br>
    <?php get_footer(); ?>
</body>

</html>