<?php

class Beneficiaries {

        private $wpdb;
        private $table_name;

        public function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;
            $this->table_name = $this->wpdb->prefix . "beneficiaries";
        }

        public function format($data){
            return isset($data) && trim($data) !== "" ? $data : 'null';
        }

        public function format_int($data){
            $aux = trim(str_replace('"', "", $data));
            return intval($aux);
        }

        public function format_data($data){
            $aux = trim(str_replace('"', "", $data));
            $data = implode('-', array_reverse(explode('/', $aux)));
            return '"' . $data . '"';
        }

        public function is_valid($data){
            return isset($data) && trim($data) !== "" ? $data : null;
        }

        public function store($file_path){
            
            $aux = 0;
            $query = null;
            $fn = fopen($file_path, "r");

            if ($fn) {

                $delete = $this->wpdb->query("TRUNCATE TABLE $this->table_name");

                $query = "INSERT INTO {$this->table_name}
                            (id,
                            password,
                            name,
                            cpf,
                            address,
                            complement,
                            neighborhood,
                            city,
                            fiscal_code,
                            phone,
                            cell_phone,
                            plan,
                            admission,
                            lack,
                            antepenultimate_payment_format,
                            antepenultimate_payment,
                            penultimate_payment_format,
                            penultimate_payment,
                            last_payment_format,
                            last_payment) VALUES ";

                while (($line = fgets($fn)) !== false) {
                    
                    $aux++;
                    $beneficiary = explode(';', utf8_encode($line));

                    $data = [
                        is_valid($beneficiary[0])  ? $this->format_int($beneficiary[0]) : 'null', 
                        is_valid($beneficiary[1])  ? $this->format_int($beneficiary[1]) : 'null', 
                        is_valid($beneficiary[2])  ? $this->format($beneficiary[2]) : 'null', 
                        is_valid($beneficiary[3])  ? $this->format($beneficiary[3]) : 'null', 
                        is_valid($beneficiary[4])  ? $this->format($beneficiary[4]) : 'null',
                        is_valid($beneficiary[5])  ? $this->format($beneficiary[5]) : 'null',
                        is_valid($beneficiary[6])  ? $this->format($beneficiary[6]) : 'null',
                        is_valid($beneficiary[7])  ? $this->format($beneficiary[7]) : 'null', 
                        is_valid($beneficiary[8])  ? $this->format($beneficiary[8]) : 'null', 
                        is_valid($beneficiary[9])  ? $this->format($beneficiary[9]) : 'null', 
                        is_valid($beneficiary[10]) ? $this->format($beneficiary[10]) : 'null', 
                        is_valid($beneficiary[11]) ? $this->format($beneficiary[11]) : 'null', 
                        is_valid($beneficiary[12]) ? $this->format_data($beneficiary[12]) : 'null',
                        is_valid($beneficiary[13]) ? $this->format_data($beneficiary[13]) : 'null',
                        is_valid($beneficiary[14]) ? $this->format($beneficiary[14]) : 'null',
                        is_valid($beneficiary[15]) ? $this->format_data($beneficiary[15]) : 'null',
                        is_valid($beneficiary[16]) ? $this->format($beneficiary[16]) : 'null', 
                        is_valid($beneficiary[17]) ? $this->format_data($beneficiary[17]) : 'null',
                        is_valid($beneficiary[18]) ? $this->format($beneficiary[18]) : 'null',
                        is_valid($beneficiary[19]) ? $this->format_data($beneficiary[19]) : 'null'
                    ];

                    $query .= '(' . implode(",", $data) . '),';
                }

                $query = substr($query,0,-1) . ';';

            } else {
                return false;
            }

            if(!isset($query)) return false;

            if($query){
                return $this->wpdb->query($query);
            }
        }

    }
?>