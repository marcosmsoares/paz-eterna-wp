<?php 
/*
Template Name: Login User
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <?php
        
        session_start();

        if(isset($_GET['logout'])){
            $_SESSION['beneficiary'] = null;
        }
        
        if(isset($_SESSION['beneficiary'])){
            header("Location: /informacoes-do-usuario/"); 
        }

        if(isset($_POST) && count($_POST) > 0){
            
            global $wpdb;
            $table_name = $wpdb->prefix . "beneficiaries";
    
            $login = intval($_POST['login']);
            $password = intval($_POST['password']);

            $query = $wpdb->prepare("SELECT * FROM {$table_name} WHERE id = %d AND password = %d LIMIT 1", $login, $password);
            $user = $wpdb->get_row($query);

            if($user){

                $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}dependents WHERE beneficiary_id = %d", $login);
                $dependents = $wpdb->get_results($query);
                $user->dependents = $dependents;

                $_SESSION['beneficiary'] = $user;
                header("Location: /informacoes-do-usuario/"); 
            }
        }
    ?>
</head>

<body>
    <?php get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Acesse seu plano</h5>
                        <form class="form-signin" method="post" onsubmit="loadButton()">
                            <div class="form-label-group">
                                <input type="text" id="inputLogin" name="login" class="form-control" placeholder="Login"
                                    required autofocus>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Senha"
                                    required>
                            </div>

                            <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase btn-login" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Entrando..." type="submit">Entrar</button>
                            <hr class="my-4">
                            <p class="text-center">Esqueceu a senha? Ligue para 3257-4055 / 3495-5992.</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
</body>

<script>
    function loadButton() {
        var button = document.getElementsByClassName("btn-login")[0];
        var textLoadingButton = button.dataset.loadingText;
        button.innerHTML = textLoadingButton;
        button.disabled = true;
    }
</script>

</html>